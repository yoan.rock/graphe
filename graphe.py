'''
Dossier :
    1 - Algorithme
    2 - Pseudo Code
    3 - Code


'''


def graphToMatAdjacente (graph):
    matAdjacente = []

    for i in range(0, len(graph)):
        ligneMat = [0] * len(graph)

        for j in range(0, len(graph[i])):
            ligneMat[graph[i][j]] = 1

        matAdjacente.append(ligneMat)

    return matAdjacente

def afficherMatAdj(matAdjacente):
    for i in range(0, len(matAdjacente)):
        for j in range(0, len(matAdjacente[i])):
            print (matAdjacente[i][j], end='')
        print()

def algoRoyWarshallAvecMatAdj(matAdjacente):
    newMat = matAdjacente

    #Parcours des différents sommets
    for sommet in range(0, len(matAdjacente)):

        for successeur in range(0, len(matAdjacente[sommet])):

            #Si c'est bien un successeur du sommet
            if matAdjacente[sommet][successeur] == 1:

                # On ajoute la ligne de sommet aux predecesseur
                for predecesseur in range(0, len(matAdjacente)):

                    # Si c'est bien un predecesseur du sommet
                    if matAdjacente[predecesseur][sommet] == 1 :


                        newMat[predecesseur][successeur] = 1

    afficherMatAdj(newMat)

    return newMat




def algoRoyWarshallAvecList(graphe):
    for sommet in range(0, len(graphe)):
        for predecesseur in range(0, len(graphe)):
            if sommet in graphe[predecesseur]:

                for successeur in range(0, len(graphe[sommet])):

                    if graphe[sommet][successeur] not in graphe[predecesseur]:
                        graphe[predecesseur].append(graphe[sommet][successeur])

    return graphe

listSom = []
def parcoursEnProfondeur(sommet, graphe):
    print(sommet," ", end='')
    listSom.append(sommet)

    for i in range (0, len(graphe[sommet])):
        if graphe[sommet][i] not in listSom:
            parcoursEnProfondeur(graphe[sommet][i], graphe)


def graphTranspose (graphe):
    newGrah = []

    for i in range(0, len(graphe)):
        newGrah[i] = []
        for j in range(0, len(graphe[i])):
            return

def fortementConnexe(graphe):
    listSom.clear()
    parcoursEnProfondeur(graphe)

    listInverse = list(reversed(listSom))




def main():
    graphe = [ [1, 2], [2], [3], [4], []]
    matAdj = graphToMatAdjacente(graphe)
    afficherMatAdj(matAdj)
    print("----------------------------")
    algoRoyWarshallAvecMatAdj(matAdj)
    algoRoyWarshallAvecList(graphe)

    parcoursEnProfondeur(0, graphe)



if __name__ == "__main__":
    main()